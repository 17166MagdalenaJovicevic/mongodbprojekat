namespace MongoDBServer.Models
{
    public class Registracija
    {
        public string KorisnickoIme { get; set; }
        public string Sifra { get; set; }
        public string PotvrdjenaSifra { get; set; }

        public string NovaSifra { get; set; }
    }

}