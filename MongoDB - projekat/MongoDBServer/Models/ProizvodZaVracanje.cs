using System.Collections.Generic;

namespace MongoDBServer.Models
{
    public class ProizvodZaVracanje
    {

        public string Id { get; set; }
        public string SifraProizvoda { get; set; }

        public string Naziv { get; set; }

        public string Kategorija { get; set; }

        public int? Kolicina { get; set; }

        public List<Cena> Cene { get; set; }

        public byte[] Slika { get; set; }   // max 7MB

        public string Velicina { get; set; }    // duzina visina ...

        public string Opis { get; set; }

        public List<VelicinaKolicina> VelicineKolicine { get; set; }

        public List<ModelKolicina> Modeli { get; set; }

    }
}