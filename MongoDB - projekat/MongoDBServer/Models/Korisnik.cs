using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace MongoDBServer.Models
{
    public class Korisnik
    {
        public ObjectId? Id { get; set; }

        public string KorisnickoIme { get; set; }

        public string Sifra { get; set; }

        public string Token { get; set; }

        public List<MongoDBRef> Omiljeno { get; set; }

    }

}