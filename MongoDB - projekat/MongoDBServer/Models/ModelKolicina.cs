using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MongoDBServer.Models
{
    public class ModelKolicina
    {
        public string Id { get; set; }

        public string Model { get; set; }

        public int Kolicina { get; set; }

    }

}