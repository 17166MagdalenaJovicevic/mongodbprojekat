using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MongoDBServer.Models
{
    public class StringReq
    {
        public ObjectId Id { get; set; }
        public string Text { get; set; }

        public int? Sifra { get; set; }
    }
}