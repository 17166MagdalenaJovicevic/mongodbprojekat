using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MongoDBServer.Models
{
    public class VelicinaKolicina
    {
        public string Id { get; set; }

        public string Velicina { get; set; }

        public int Kolicina { get; set; }

    }

}