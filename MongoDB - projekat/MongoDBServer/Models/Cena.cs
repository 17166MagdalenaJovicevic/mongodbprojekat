using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MongoDBServer.Models
{
    public class Cena
    {
        public string Id { get; set; }

        public float? Vrednost { get; set; }

        public string Valuta { get; set; }

    }

}