namespace MongoDBServer.Models {
    public class DatabaseSettings : IDatabaseSettings
    {
        public string CollectionName { get; set; }
        public string KategorijeCollectionName { get; set; }
        public string KorisniciCollectionName { get; set; }
        public string TokeniCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IDatabaseSettings
    {
        string CollectionName { get; set; }
        string KategorijeCollectionName { get; set; }
        string KorisniciCollectionName { get; set; }
        string TokeniCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}

