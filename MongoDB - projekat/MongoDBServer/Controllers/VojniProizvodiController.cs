﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using MongoDBServer.Models;
using MongoDB.Bson;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.Primitives;
using System.Linq.Expressions;

namespace MongoDBServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VojniProizvodiController : ControllerBase
    {


        private readonly ILogger<VojniProizvodiController> _logger;
        private readonly MongoClient client;
        private readonly IMongoDatabase database;
        private readonly IMongoCollection<Proizvod> proizvodi;
        private readonly IMongoCollection<StringReq> kategorije;
        private readonly IMongoCollection<Korisnik> korisnici;
        private readonly IMongoCollection<Korisnik> tokeni;

        public VojniProizvodiController(ILogger<VojniProizvodiController> logger, IDatabaseSettings settings)
        {
            _logger = logger;
            client = new MongoClient(settings.ConnectionString);
            database = client.GetDatabase(settings.DatabaseName);
            proizvodi = database.GetCollection<Proizvod>(settings.CollectionName);
            kategorije = database.GetCollection<StringReq>(settings.KategorijeCollectionName);
            korisnici = database.GetCollection<Korisnik>(settings.KorisniciCollectionName);
            tokeni = database.GetCollection<Korisnik>(settings.TokeniCollectionName);
            if (kategorije.CountDocuments(x => true) == 0) {
                List<StringReq> lista = new List<StringReq>();
                lista.Add(new StringReq { Text = "Odeća"});
                lista.Add(new StringReq { Text = "Obuća"});
                lista.Add(new StringReq { Text = "Oprema"});
                lista.Add(new StringReq { Text = "Sifra", Sifra = 0});
                kategorije.InsertMany(lista);
            }
        }

        private async Task<bool> Token(string Username, string authorization) {

            var token  = (await tokeni.FindAsync(tok => tok.KorisnickoIme == Username && tok.Token == authorization)).FirstOrDefault();
            bool fleg = false;

            if (token != default) {
                if (token.Id.Value.CreationTime.AddHours(6).Ticks < DateTime.UtcNow.Ticks) {
                    fleg = false;
                }
                else {
                    fleg = true;

                } 
            }
            else {
                fleg = false;
            }

            var sad = DateTime.UtcNow.Ticks;

            var zaBrisanje = (await tokeni.FindAsync(x => x.KorisnickoIme == Username)).ToList();

            if (zaBrisanje.Count > 0) {
                foreach(var tok in zaBrisanje) {
                    if (tok.Id.Value.CreationTime.AddHours(6).Ticks < DateTime.UtcNow.Ticks) {
                        tokeni.DeleteOne(x => x.Id == tok.Id);
                    }
                }
            }      
                
            return fleg;

        }

        [HttpPost]
        [Route("Registracija")]
        public async Task<string> Registracija(Registracija podaci) {

            if (podaci.KorisnickoIme.Length < 8 || podaci.KorisnickoIme.Contains(" ")) {
                return "Korisnicko ime mora imati bar 8 karaktera ili sadrzi razmake";
            }

            if ((await korisnici.FindAsync(x => x.KorisnickoIme == podaci.KorisnickoIme)).FirstOrDefault() != default) {
                return "Username zauzet";
            }

            if (podaci.Sifra.Length < 8) {
                return "Sifra mora imati bar 8 karaktera";
            }

            if (podaci.Sifra != podaci.PotvrdjenaSifra) {
                return "Sifra i potvrdjena sifra se ne poklapaju";
            }

            HashAlgorithm sha = SHA256.Create();
            
            byte[] result = sha.ComputeHash(Encoding.ASCII.GetBytes(podaci.Sifra));
            podaci.Sifra = Encoding.ASCII.GetString(result);

            var korisnik = new Korisnik { KorisnickoIme = podaci.KorisnickoIme, Sifra = podaci.Sifra };
            korisnik.Id = ObjectId.GenerateNewId();

            korisnici.InsertOne(korisnik);

            return "Uspesna registracija";

        }

        [HttpPost]
        [Route("PromeniSifru")]
        public async Task<string> PromeniSifru(Registracija podaci) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(podaci.KorisnickoIme, authorization);
            
            if (fleg == false) {
                return "Los token";
            }

            Korisnik korisnik = null;

            if ((korisnik = (await korisnici.FindAsync(x => x.KorisnickoIme == podaci.KorisnickoIme)).FirstOrDefault()) == default) {
                return "Korisnik sa ovim korisnickim imenom ne postoji";
            }

            HashAlgorithm sha = SHA256.Create();
            
            byte[] result = sha.ComputeHash(Encoding.ASCII.GetBytes(podaci.Sifra));
            podaci.Sifra = Encoding.ASCII.GetString(result);

            if (korisnik.Sifra != podaci.Sifra) {
                return "Netacna sifra";
            }

            if (podaci.NovaSifra.Length < 8) {
                return "Sifra mora imati bar 8 karaktera";
            }

            if (podaci.NovaSifra != podaci.PotvrdjenaSifra) {
                return "Sifra i potvrdjena sifra se ne poklapaju";
            }

            result = sha.ComputeHash(Encoding.ASCII.GetBytes(podaci.NovaSifra));
            podaci.NovaSifra = Encoding.ASCII.GetString(result);

            korisnici.UpdateOne(k => k.KorisnickoIme == podaci.KorisnickoIme, MongoDB.Driver.Builders<Korisnik>.Update.Set("Sifra", podaci.NovaSifra));

            tokeni.DeleteMany(x => x.KorisnickoIme == podaci.KorisnickoIme);

            var random = new Random((int)DateTime.UtcNow.Ticks);
            string token = "";
            for(int i = 0; i < 20; i++) {
                token += (new Random((int)DateTime.UtcNow.Ticks).Next().ToString());
            }

            Korisnik tokenNovi = new Korisnik { KorisnickoIme = podaci.KorisnickoIme, Token = token };
            tokenNovi.Id = ObjectId.GenerateNewId();

            tokeni.InsertOne(tokenNovi);

            return token;


        }

        [HttpPost]
        [Route("Prijava")]
        public async Task<string> Prijava(Registracija podaci) {

            Korisnik korisnik = null;

            if ((korisnik = (await korisnici.FindAsync(x => x.KorisnickoIme == podaci.KorisnickoIme)).FirstOrDefault()) == default) {
                return "Korisnik sa ovim korisnickim imenom ne postoji";
            }

            

            HashAlgorithm sha = SHA256.Create();
            
            byte[] result = sha.ComputeHash(Encoding.ASCII.GetBytes(podaci.Sifra));
            podaci.Sifra = Encoding.ASCII.GetString(result);

            if (korisnik.Sifra != podaci.Sifra) {
                return "Netacna sifra";
            }

            var random = new Random((int)DateTime.UtcNow.Ticks);
            string token = "";
            for(int i = 0; i < 20; i++) {
                token += (new Random((int)DateTime.UtcNow.Ticks).Next().ToString());
            }

            Korisnik tokenNovi = new Korisnik { KorisnickoIme = podaci.KorisnickoIme, Token = token };
            tokenNovi.Id = ObjectId.GenerateNewId();

            tokeni.InsertOne(tokenNovi);

            return token;

        }

        [HttpGet]
        [Route("VratiKategorije")]
        public List<StringReq> VratiKategorijeProizvoda() {
            var sveKategorije = kategorije.Find(x => x.Text != "Sifra").ToList();
            return sveKategorije;
        }

        [HttpPost]
        [Route("DodajProizvod/{noviTip}")]
        public async Task<string> DodajProizvod([FromBody] Proizvod p, [FromRoute] string noviTip) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token("admin", authorization);
            
            if (fleg == false) {
                return "Los token";
            }

            if (noviTip == "true") {
                kategorije.InsertOne(new StringReq { Text = p.Kategorija});
            }

            if (p.Kategorija != "Odeća" && p.Kategorija != "Obuća") {
                int br = 0;
                for (int i = 0; i < p.VelicineKolicine.Count; i++) {
                    br += p.VelicineKolicine[i].Kolicina;
                }
                if (p.Kolicina < br) {
                    return "Ukupna kolicina ne moze biti manja od zbira kolicina pojedinacnih velicina";
                }
                br = 0;
                for (int i = 0; i < p.Modeli.Count; i++) {
                    br += p.Modeli[i].Kolicina;
                }
                if (p.Kolicina < br) {
                    return "Ukupna kolicina ne moze biti manja od zbira kolicina pojedinacnih modela";
                }
            }

            if (p.Id == null) {
                p.Id = ObjectId.GenerateNewId();
            }
            for(int i = 0; i < p.Cene.Count; i++) {
                if (p.Cene[i].Id == null)
                    p.Cene[i].Id = ObjectId.GenerateNewId().ToString();
            }
            for(int i = 0; i < p.VelicineKolicine.Count; i++) {
                if (p.VelicineKolicine[i].Id == null)
                    p.VelicineKolicine[i].Id = ObjectId.GenerateNewId().ToString();
            }
            for(int i = 0; i < p.Modeli.Count; i++) {
                if (p.Modeli[i].Id == null)
                    p.Modeli[i].Id = ObjectId.GenerateNewId().ToString();
            }


            var ks = (await kategorije.FindAsync<StringReq>(k => k.Text == "Sifra")).FirstOrDefault();
            p.SifraProizvoda = ks.Sifra.ToString();
            ks.Sifra++;
            kategorije.UpdateOne(k => k.Text == "Sifra", MongoDB.Driver.Builders<StringReq>.Update.Set("Sifra",ks.Sifra));

            proizvodi.InsertOne(p);


            return p.SifraProizvoda;
            //proizvodi.InsertOne(p);
            //return proizvodi.Find<Proizvod>(pr => pr.Naziv == p.Naziv).FirstOrDefault();
        }

        [HttpGet]
        [Route("VratiProizvode")]
        public async Task<List<Proizvod>> VratiProizvode(){
            
            return await proizvodi.Find(x=>true).Limit(20).ToListAsync();
            
        }

        [HttpGet]
        [Route("VratiBrojProizvoda")]
        public async Task<long> VratiBrojProizvoda(){
            long brojProzivoda = await proizvodi.CountDocumentsAsync(x=> true);
            return brojProzivoda;
        }

        [HttpGet]
        [Route("Vrati6Proizvoda/{polozaj}")]
        public async Task<List<Proizvod>> Vrati6Proizvoda([FromRoute] int polozaj){
            long brojProzivoda = await proizvodi.CountDocumentsAsync(x=> true);

            return await proizvodi.Find(x=>true).Skip(polozaj).Limit(6).ToListAsync();
        }

        /*[HttpGet]
        [Route("VratiPretrazeneProizvode/{textPretrage}")]
        public async Task<List<Proizvod>> VratiPretrazeneProizvode([FromRoute] string textPretrage){
            return await proizvodi.Find(x => x.Naziv.Contains(textPretrage)).ToListAsync();
        }*/

        [HttpPost]
        [Route("VratiBrojPretrazeneProizvode/{textPretrage}/{fleg}")]
        public async Task<long> VratiBrojPretrazeneProizvode([FromRoute] string textPretrage, [FromRoute] string fleg, [FromBody] List<string> kategorije) {

            return await proizvodi.CountDocumentsAsync(FormirajFilter(textPretrage, kategorije, fleg));

        }

        private FilterDefinition<Proizvod> FormirajFilter(string textPretrage, List<string> kategorije, string fleg) {

            FilterDefinition<Proizvod> filter = null;

            if (fleg == "true") {   // da bi se vrsila pretraga samo ako je korisnik ukucao nesto

                List<string> reci = textPretrage.Split(" ").ToList();


                foreach(var rec in reci) {
                    if (filter == null) {
                        filter = Builders<Proizvod>.Filter.Regex("Naziv","/" + rec + "/i");
                    }
                    else {
                        filter = filter | Builders<Proizvod>.Filter.Regex("Naziv","/" + rec + "/i");
                    }
                
                }
            }
            
            
            

            FilterDefinition<Proizvod> filter2 = null;
            if (kategorije.Count > 0) {

                for(int i = 0; i < kategorije.Count; i++) {

                    if (i == 0) {

                        filter2 = Builders<Proizvod>.Filter.Eq("Kategorija", kategorije[i]);
                    }
                    else {

                        filter2 = filter2 | Builders<Proizvod>.Filter.Eq("Kategorija", kategorije[i]);

                    }
                    
                }
                
            }

            if (filter == null) {
                filter = filter2;
            }
            else if (filter2 != null) {
                filter = filter & filter2;
            }

            if (filter == null) {
                filter = Builders<Proizvod>.Filter.Empty;
            }

            return filter;

        }

        [HttpPost]
        [Route("Vrati6PretrazeneProizvode/{textPretrage}/{polozaj}/{sortira}/{fleg}")]
        public async Task<List<ProizvodZaVracanje>> Vrati6PretrazeneProizvode([FromRoute] string textPretrage, [FromRoute] int polozaj, [FromBody] List<string> kategorije, [FromRoute] string sortira, [FromRoute] string fleg) {


            
            var filter = FormirajFilter(textPretrage, kategorije, fleg);
            

            List<Proizvod> lista = new List<Proizvod>();

            if (sortira == "cr") {  // cena rastuce
                lista = await proizvodi.Find(filter).SortBy(p => p.Cene[0].Vrednost).Skip(polozaj).Limit(6).ToListAsync();
            }
            else if (sortira == "co") { // cena opadajuce
                lista = await proizvodi.Find(filter).SortByDescending(p => p.Cene[0].Vrednost).Skip(polozaj).Limit(6).ToListAsync();
            }
            else if (sortira == "nr") { // naziv rastuce
                lista = await proizvodi.Find(filter).SortBy(p => p.Naziv).Skip(polozaj).Limit(6).ToListAsync();
            }
            else if (sortira == "no") { // naziv opadajuce
                lista = await proizvodi.Find(filter).SortByDescending(p => p.Naziv).Skip(polozaj).Limit(6).ToListAsync();
            }
            else {
                lista = await proizvodi.Find(filter).Skip(polozaj).Limit(6).ToListAsync();
            }

            /*if (filter == Builders<Proizvod>.Filter.Empty) {
                return lista;
            }*/

            /*if (lista.Count < 6) {

                int br = 6;
                br = br - lista.Count;
                
                var listaNastavakAkoNijeNadjeno = new List<Proizvod>();
                if (sortira == "cr") {  // cena rastuce
                    listaNastavakAkoNijeNadjeno = await proizvodi.Find(Builders<Proizvod>.Filter.Not(filter)).SortBy(p => p.Cene[0].Vrednost).Skip(polozaj).Limit(br).ToListAsync();
                }
                else if (sortira == "co") { // cena opadajuce
                    listaNastavakAkoNijeNadjeno = await proizvodi.Find(Builders<Proizvod>.Filter.Not(filter)).SortByDescending(p => p.Cene[0].Vrednost).Skip(polozaj).Limit(br).ToListAsync();
                }
                else if (sortira == "nr") { // naziv rastuce
                    listaNastavakAkoNijeNadjeno = await proizvodi.Find(Builders<Proizvod>.Filter.Not(filter)).SortBy(p => p.Naziv).Skip(polozaj).Limit(br).ToListAsync();
                }
                else if (sortira == "no") { // naziv opadajuce
                    listaNastavakAkoNijeNadjeno = await proizvodi.Find(Builders<Proizvod>.Filter.Not(filter)).SortByDescending(p => p.Naziv).Skip(polozaj).Limit(br).ToListAsync();
                }
                else {
                    listaNastavakAkoNijeNadjeno = await proizvodi.Find(Builders<Proizvod>.Filter.Not(filter)).Skip(polozaj).Limit(br).ToListAsync();
                } 

                lista.AddRange(listaNastavakAkoNijeNadjeno);

            }*/

            var listaZaVracanje = new List<ProizvodZaVracanje>();

            if (lista.Count > 0) {
                foreach(var pr in lista) {
                    ProizvodZaVracanje prpom = new ProizvodZaVracanje { Id = pr.Id.ToString(), SifraProizvoda = pr.SifraProizvoda,
                    Naziv = pr.Naziv, Kategorija = pr.Kategorija, Kolicina = pr.Kolicina, Cene = pr.Cene, Slika = pr.Slika, Velicina = pr.Velicina, 
                    Opis = pr.Opis, VelicineKolicine = pr.VelicineKolicine, Modeli = pr.Modeli};
                    listaZaVracanje.Add(prpom);
                }
            }
            

            return listaZaVracanje;

        }

        [HttpPost]
        [Route("LajkujOdljakuj")]
        public async Task<string> LajkujOdljakuj(Lajkovanje podaci) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(podaci.KorisnickoIme, authorization);
            
            if (fleg == false) {
                return "Los token";
            }

            var id = ObjectId.Parse(podaci.IdProizvoda);

            Proizvod proizvodUBazi = proizvodi.Find(p => p.Id == id).FirstOrDefault();

            if(proizvodUBazi == null)
                return "proizvod ne postoji";

            Korisnik korisnik = await korisnici.Find(k => k.KorisnickoIme == podaci.KorisnickoIme).FirstAsync();
        
            if(korisnik == null)
                return "korisnik ne postoji";

            if(korisnik.Omiljeno == null){
                korisnik.Omiljeno = new List<MongoDBRef>();
                var filter = Builders<Korisnik>.Filter.Eq(s => s.Id, korisnik.Id);
                var result = await korisnici.ReplaceOneAsync(filter, korisnik);
            }

            var proizvodRef = new MongoDBRef("Proizvodi", proizvodUBazi.Id);

            bool nadjen = false;
            int br = -1;
            for (int i = 0; i < korisnik.Omiljeno.Count; i++) {
                if (korisnik.Omiljeno[i].Id.ToString() == proizvodUBazi.Id.ToString()) {
                    nadjen = true;
                    br = i;
                    break;
                }
            }

            if (nadjen == true) {
                korisnik.Omiljeno.Remove(korisnik.Omiljeno[br]);
            }
            else {
                korisnik.Omiljeno.Add(proizvodRef);
            }

            await korisnici.UpdateOneAsync(k => k.KorisnickoIme == podaci.KorisnickoIme, MongoDB.Driver.Builders<Korisnik>.Update.Set("Omiljeno",korisnik.Omiljeno));
            
            if (nadjen == true) {
                return "odlajkovano";
            }
            else {
                return "lajkovano";
            }

        }


        [HttpPost]
        [Route("DaLiJeLajkovano")]
        public async Task<string> DaLiJeLajkovano(Lajkovanje podaci) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(podaci.KorisnickoIme, authorization);
            
            if (fleg == false) {
                return "Los token";
            }

            var id = ObjectId.Parse(podaci.IdProizvoda);

            var filter = Builders<Proizvod>.Filter.Eq(s => s.Id, id);
            Proizvod prozivodUBazi = proizvodi.Find(filter).FirstOrDefault();
            if (prozivodUBazi == default)
                return "proizvod ne postoji";

            Korisnik korisnik = await korisnici.Find(k => k.KorisnickoIme == podaci.KorisnickoIme).SingleAsync();

            if (korisnik == null)
                return "korisnik ne postoji";

            
            if (korisnik.Omiljeno == null) {

                korisnik.Omiljeno = new List<MongoDBRef>();
                var filter2 = Builders<Korisnik>.Filter.Eq(s => s.Id, korisnik.Id);
                var result = await korisnici.UpdateOneAsync(filter2, MongoDB.Driver.Builders<Korisnik>.Update.Set("Omiljeno",korisnik.Omiljeno));

            }
               
            
            var proizvodRef = new MongoDBRef("Proizvodi", prozivodUBazi.Id);
            // korisnik.Omiljeno.Add(proizvodRef);
            // var filter3 = Builders<Korisnik>.Filter.Eq(s => s.Id, korisnik.Id);
            // var result1 = await korisnici.ReplaceOneAsync(filter3, korisnik);
            
            bool nadjen = false;
            korisnik.Omiljeno.ForEach(p => {
                if(p.Id.ToString() == prozivodUBazi.Id.ToString()) {
                    nadjen = true;
                }
                    
            });

            if(nadjen){
                return "lajkovano";
            }   
            else return "nije lajkovano";

        }
        
        [HttpPost]
        [Route("VratiBrojOmiljenihProizvoda")]
        public async Task<long> VratiBrojOmiljenihProizvoda(Lajkovanje podaci) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(podaci.KorisnickoIme, authorization);
            
            if (fleg == false) {
                return -1;
            }

            Korisnik korisnik = await korisnici.Find(k => k.KorisnickoIme == podaci.KorisnickoIme).SingleAsync();
            if (korisnik == null) {
                return -2;
            }

            return vratiBrojOmiljenihProizvoda(korisnik);
            
        }

        private long vratiBrojOmiljenihProizvoda(Korisnik korisnik){

            if(korisnik.Omiljeno != null)
                return korisnik.Omiljeno.Count();
            else return 0;
        }

        [HttpPost]
        [Route("Vrati6OmiljenihProizvoda/{polozaj}")]
        public async Task<List<ProizvodZaVracanje>> Vrati6OmiljenihProizvoda(Lajkovanje podaci, [FromRoute] int polozaj) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token(podaci.KorisnickoIme, authorization);

            if (fleg == false) {
                return null;
            }

            Korisnik korisnik = await korisnici.Find(k => k.KorisnickoIme == podaci.KorisnickoIme).SingleAsync();
            if (korisnik == null) {
                return null;
            }

            long brojProizvoda = vratiBrojOmiljenihProizvoda(korisnik);

            if(brojProizvoda == 0)
                return new List<ProizvodZaVracanje>();

            List<BsonValue> Ids = new List<BsonValue>();
            
            for (int i = polozaj; i < korisnik.Omiljeno.Count && i < polozaj + 6; i++) {

                Ids.Add(korisnik.Omiljeno[i].Id);

            }

            var filter = Builders<Proizvod>.Filter.In("Id",Ids);

            var lista = proizvodi.Find(filter).ToList();

            List<ProizvodZaVracanje> listaZaVracanje = new List<ProizvodZaVracanje>();

            if (lista.Count > 0) {
                foreach(var pr in lista) {
                    ProizvodZaVracanje prpom = new ProizvodZaVracanje { Id = pr.Id.ToString(), SifraProizvoda = pr.SifraProizvoda,
                    Naziv = pr.Naziv, Kategorija = pr.Kategorija, Kolicina = pr.Kolicina, Cene = pr.Cene, Slika = pr.Slika, Velicina = pr.Velicina, 
                    Opis = pr.Opis, VelicineKolicine = pr.VelicineKolicine, Modeli = pr.Modeli};
                    listaZaVracanje.Add(prpom);
                }
            }
            

            return listaZaVracanje;

        }

        [HttpPut]
        [Route("IzmeniProizvod")]
        public async Task<string> IzmeniProizvod([FromBody]ProizvodZaVracanje izmenjenProizvod){
            
            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token("admin", authorization);

            if (fleg == false) {
                return "Los token";
            }

            var objectID = new ObjectId(izmenjenProizvod.Id);
            var proizvod = await proizvodi.Find(pr => pr.Id == objectID).FirstOrDefaultAsync();

            if(proizvod == default){
                return "Proizvod ne posotoji!";
            }

            var p = izmenjenProizvod;

            if (p.Kategorija != "Odeća" && p.Kategorija != "Obuća") {
                int br = 0;
                for (int i = 0; i < p.VelicineKolicine.Count; i++) {
                    br += p.VelicineKolicine[i].Kolicina;
                }
                if (p.Kolicina < br) {
                    return "Ukupna kolicina ne moze biti manja od zbira kolicina pojedinacnih velicina";
                }
                br = 0;
                for (int i = 0; i < p.Modeli.Count; i++) {
                    br += p.Modeli[i].Kolicina;
                }
                if (p.Kolicina < br) {
                    return "Ukupna kolicina ne moze biti manja od zbira kolicina pojedinacnih modela";
                }
            }

            Proizvod saIzmenama = new Proizvod();
            saIzmenama.Id = new ObjectId(izmenjenProizvod.Id);
            saIzmenama.SifraProizvoda = izmenjenProizvod.SifraProizvoda;
            saIzmenama.Naziv = izmenjenProizvod.Naziv;
            saIzmenama.Kategorija = izmenjenProizvod.Kategorija;
            saIzmenama.Slika = izmenjenProizvod.Slika;
            saIzmenama.Kolicina = izmenjenProizvod.Kolicina;
            saIzmenama.Opis = izmenjenProizvod.Opis;
            saIzmenama.Velicina = izmenjenProizvod.Velicina;
            
            saIzmenama.Cene = new List<Cena>();

            foreach(var c in izmenjenProizvod.Cene) {
                if (c.Id == null) {
                    c.Id = ObjectId.GenerateNewId().ToString();
                    
                }
                saIzmenama.Cene.Add(c);
            }

            saIzmenama.VelicineKolicine = new List<VelicinaKolicina>();

            foreach(var c in izmenjenProizvod.VelicineKolicine) {
                if (c.Id == null) {
                    c.Id = ObjectId.GenerateNewId().ToString();
                    
                }
                saIzmenama.VelicineKolicine.Add(c);
            }

            saIzmenama.Modeli = new List<ModelKolicina>();

            foreach(var c in izmenjenProizvod.Modeli) {
                if (c.Id == null) {
                    c.Id = ObjectId.GenerateNewId().ToString();
                    
                }
                saIzmenama.Modeli.Add(c);
            }



            var filter = Builders<Proizvod>.Filter.Eq(s => s.Id, objectID);
            var result = await proizvodi.ReplaceOneAsync(filter, saIzmenama);
            return "Uspesno";

        }


        [HttpPost]
        [Route("ObrisiProizvod/{id}")]
        public async Task<string> ObrisiProizvod([FromRoute] string id) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            bool fleg = await Token("admin", authorization);

            if (fleg == false) {
                return "Los token";
            }

            var obj = new ObjectId(id);

            proizvodi.DeleteOne(x => x.Id == obj);

            return "Uspesno";

        }

        [HttpGet]
        [Route("VratiProizvod/{sifraProizvoda}")]

        public ProizvodZaVracanje VratiProizvod([FromRoute]string sifraProizvoda){

            var pr = proizvodi.Find(p=>p.SifraProizvoda == sifraProizvoda).SingleOrDefault();

            ProizvodZaVracanje prpom = new ProizvodZaVracanje { Id = pr.Id.ToString(), SifraProizvoda = pr.SifraProizvoda,
                    Naziv = pr.Naziv, Kategorija = pr.Kategorija, Kolicina = pr.Kolicina, Cene = pr.Cene, Slika = pr.Slika, Velicina = pr.Velicina, 
                    Opis = pr.Opis, VelicineKolicine = pr.VelicineKolicine, Modeli = pr.Modeli};

            if(prpom == default){
                return null;
            }

            return prpom;
        }

       /* [HttpGet]
        [Route("VratiProizvodePoTekstuPretrage/{tekstPretrage}")]
        public async Task<List<Proizvod>> VratiProizvodePoImenu([FromRoute]string tekstPretrage){
            
          /*  string[] reciPretrage = tekstPretrage.Split(' ');
            var lista = new List<Proizvod>();
            foreach(var rec in reciPretrage) {
                var query = proizvodi.AsQueryable()
                                 .Where(p=> p.Naziv.Contains(rec))
                                 .Select(p=>p);


                foreach(var p in query.ToList<Proizvod>()) {
                    lista.Add(p);
                }
            }


           // var a = await proizvodi.Indexes.CreateOneAsync(new CreateIndexModel<Proizvod>(Builders<Proizvod>.IndexKeys.Text(x => x.Naziv)));
            //return await proizvodi.Find(Builders<Proizvod>.Filter.Text("(.)*"+tekstPretrage+"(.)*")).ToListAsync();
           // return lista;
        }*/
        /*[HttpPost]
        [Route("DodajOdecu")]
        public Proizvod DodajOdecu(Proizvod p) {
            //p.Kategorija = "Odeca";
            p.Kolicina = 0;
            if (p.VelicineKolicine.Count > 0) {
                foreach(var kol in p.VelicineKolicine) {
                    p.Kolicina += kol.Kolicina;
                }
            }
            
            proizvodi.InsertOne(p);
            return proizvodi.Find<Proizvod>(pr => pr.Naziv == p.Naziv).FirstOrDefault();
        }*/

        
    }
}
