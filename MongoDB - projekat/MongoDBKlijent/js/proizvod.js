import { Cena } from "./cena.js";
import { ModelKolicina } from "./modelKolicina.js";
import { VelicinaKolicina } from "./velicinaKolicina.js";
import { LosToken } from "./main.js";



export class Proizvod {

    constructor(id, sifra, naziv, kategorija, kolicina, cene, slika, velicina, opis, velicinekolicine, modeli){

        this.id = id;
        this.sifraProizvoda = sifra;
        this.naziv = naziv;
        this.kategorija = kategorija;
        this.kolicina = kolicina;
        this.slika = slika;
        this.cene = cene;
        this.velicina = velicina;
        this.opis = opis;
        this.velicinekolicine = velicinekolicine;
        this.modeli = modeli;


        this.webapi = sessionStorage.getItem("webapi");

    }

    PrikazProizvoda(host) {

        let kontejner = document.createElement("div");
        kontejner.classList.add("KontejnerProizvoda");

        let card = document.createElement("div");
        card.classList.add("card-body","bg-white");
        

        let slika = document.createElement("img");
        slika.classList.add("SlikaProizvoda");
        slika.classList.add("card-img-top");
        kontejner.append(card);
        //kontejner.append(slika);

        let cardTitle = document.createElement("h5");
        cardTitle.classList.add("card-title");
        cardTitle.innerHTML = this.naziv; // + " " + this.sifraProizvoda;

        card.append(cardTitle);

        let blob;
        let fajl = this.base64ToArrayBuffer(this.slika);

        if(fajl.length > 0){
        blob = new Blob([fajl],{type: "image/jpeg"});
            
        let link = document.createElement("a");
        link.target = "_blank";
        link.href = window.URL.createObjectURL(blob);

        slika.src = link.href;
        kontejner.appendChild(slika);

        }
        
        
        // let kategorija = document.createElement("div");
        // kategorija.classList.add("fw-light");
        // kategorija.innerHTML = this.kategorija;

        // card.append(kategorija);

        let cene = document.createElement("div");
        cene.classList.add("fw-strong");
        cene.innerHTML = "";
        this.cene.forEach(el => {

            cene.innerHTML += el.vrednost;
            cene.innerHTML += el.valuta + " ";
            
        });

        kontejner.appendChild(cene);

        // let kolicina = document.createElement("div");
        // kolicina.classList.add("fw-light");
        // kolicina.innerHTML ="Kolicina: "+ this.kolicina;
        // card.appendChild(kolicina);

        let naStanju = document.createElement("div");
        naStanju.style.fontFamily = "bold";
        if(this.kolicina != 0){
            naStanju.innerHTML = " NA STANJU ";
            naStanju.style.color = "white";
            naStanju.style.backgroundColor = "green";
        }
        else {
            naStanju.innerHTML = " NEMA NA STANJU ";
            naStanju.style.color = "white";
            naStanju.style.backgroundColor = "red";
        }

        kontejner.appendChild(naStanju);


        // let velicina = document.createElement("div");
        // velicina.classList.add("fw-light");
        // velicina.innerHTML ="Velicina: "+ this.velicina;
        //card.appendChild(velicina);

        // let velicineKolicine = document.createElement("div");
        // velicineKolicine.classList.add("fw-light");
        // velicineKolicine.innerHTML = "";
        // this.velicinekolicine.forEach(el=>{
        //     velicineKolicine.innerHTML += el.velicina;
        //     velicineKolicine.innerHTML += " " +el.kolicina +",";

        // })

        // card.appendChild(velicineKolicine);

        // let opis = document.createElement("p");
        // opis.classList.add("fw-light","fst-italic");
        // opis.innerHTML = this.opis.slice(0,60) + "...";

        // card.append(opis);

        kontejner.onclick = () =>{

            let kontejnerIzabranogProizvoda = document.querySelector(".container-IzabraniProizvod");

            if (kontejnerIzabranogProizvoda.style.display == "none" || document.querySelector(".SifraProizvodaNaPrikazu").ariaLabel != this.sifraProizvoda) {
                kontejnerIzabranogProizvoda.style.display = "block";
                this.prikaziIzabraniProizvod(this);
            }
            else if (document.querySelector(".SifraProizvodaNaPrikazu").ariaLabel == this.sifraProizvoda) {
                kontejnerIzabranogProizvoda.style.display = "none";
            }

            

        }

        host.append(kontejner);

    }

    base64ToArrayBuffer(base64) {

        var binaryString = window.atob(base64);
        var binaryLen = binaryString.length;
        var bytes = new Uint8Array(binaryLen);
        for (var i = 0; i < binaryLen; i++) {
           var ascii = binaryString.charCodeAt(i);
           bytes[i] = ascii;
        }
        return bytes;
    }

    async prikaziIzabraniProizvod(proizvod) {

            let kontejnerIzabranogProizvoda = document.querySelector(".IzabraniProizvod");

            while(kontejnerIzabranogProizvoda.firstChild)
                kontejnerIzabranogProizvoda.removeChild(kontejnerIzabranogProizvoda.firstChild);
            
            let sifra = document.createElement("h2");
            sifra.classList.add("SifraProizvodaNaPrikazu");
            sifra.ariaLabel = proizvod.sifraProizvoda;
            sifra.innerHTML = "Šifra proizvoda: " + proizvod.sifraProizvoda;
            kontejnerIzabranogProizvoda.appendChild(sifra);
            
            let naziv = document.createElement("h1");
            naziv.innerHTML = proizvod.naziv + " ";

            //srce
            let srce = document.createElement("img");
            srce.onmouseover = (ev) => {
                srce.style.cursor = "pointer";
            }
            srce.classList.add("srce");
            var lajkovano = await this.DaLiJeLajkovano(proizvod);
        

            srce.onclick = async () => {

                var odgovor = await this.LajkujOdlajkuj(proizvod);
                
                if(odgovor === "odlajkovano"){
                    srce.src = "resource/empty_heart.png"
                }
                else if(odgovor === "lajkovano"){
                    srce.src = "resource/full_heart.png"
                }

            }

            if(lajkovano == "lajkovano"){
                srce.src = "resource/full_heart.png";
                srce.classList.add("srce");
                naziv.appendChild(srce);
            }
            else if(lajkovano == "nije lajkovano"){
                srce.src = "resource/empty_heart.png";
                srce.classList.add("srce");
                naziv.appendChild(srce);
            }


            kontejnerIzabranogProizvoda.appendChild(naziv);

            let flexSlika = document.createElement("div");
            flexSlika.classList.add("d-flex");
            kontejnerIzabranogProizvoda.appendChild(flexSlika);

            let slika = document.createElement("img");
            slika.classList.add("SlikaProizvoda", "SlikaProizvodaPrikaz");

            let blob;
            let fajl = this.base64ToArrayBuffer(this.slika);

            if(fajl.length > 0) {

                blob = new Blob([fajl],{type: "image/jpeg"});
                    
                let link = document.createElement("a");
                link.target = "_blank";
                link.href = window.URL.createObjectURL(blob);

                slika.src = link.href;
                flexSlika.appendChild(slika);

            }

            //kolicina
            if (this.kolicina != "") {
                let velicina = document.createElement("div");
                velicina.classList.add("fw-light");
                velicina.innerHTML ="Količina: "+ this.kolicina;
                kontejnerIzabranogProizvoda.appendChild(velicina);
            }

                //velicina
                if (this.velicina != "") {
                    let velicina = document.createElement("div");
                    velicina.classList.add("fw-light");
                    velicina.innerHTML ="Veličina: "+ this.velicina;
                    kontejnerIzabranogProizvoda.appendChild(velicina);
                }
                

                let velicineKolicineSve = document.createElement("div");
                velicineKolicineSve.classList.add("d-flex","flex-column");

                
                this.velicinekolicine.forEach(el => {
                    let velicineKolicine = document.createElement("div");
                    velicineKolicine.classList.add("text-dark","bg-light","m-2","p-1");
                    velicineKolicine.style.maxWidth = "fit-content";
                    velicineKolicine.innerHTML = "";
                    velicineKolicine.innerHTML += el.velicina;
                    velicineKolicine.innerHTML += "   |   " +el.kolicina + " ";
                    velicineKolicineSve.appendChild(velicineKolicine);
                })

                kontejnerIzabranogProizvoda.appendChild(velicineKolicineSve);
                

                //modeli
                velicineKolicineSve = document.createElement("div");
                velicineKolicineSve.classList.add("d-flex","flex-column");

                
                this.modeli.forEach(el => {
                    let velicineKolicine = document.createElement("div");
                    velicineKolicine.classList.add("text-dark","bg-light","m-2","p-1");
                    velicineKolicine.style.maxWidth = "fit-content";
                    velicineKolicine.innerHTML = "";
                    velicineKolicine.innerHTML += el.model;
                    velicineKolicine.innerHTML += "   |   " +el.kolicina + " ";
                    velicineKolicineSve.appendChild(velicineKolicine);
                })

                kontejnerIzabranogProizvoda.appendChild(velicineKolicineSve);

                //opis
                let opis = document.createElement("p");
                opis.innerHTML = this.opis;
                kontejnerIzabranogProizvoda.appendChild(opis);

                let cene = document.createElement("div");
            cene.classList.add("fw-strong","bg-light","p-2","text-dark");
            cene.innerHTML = "";
            this.cene.forEach(el => {

                cene.innerHTML += el.vrednost;
                cene.innerHTML += el.valuta + " ";
                
            });

            kontejnerIzabranogProizvoda.appendChild(cene);

            


            
            
            if(localStorage.getItem("username") == "admin")
            {
                let divIzmeni = document.createElement("div");
                divIzmeni.classList.add("providno","mx-2");
                let btn = document.createElement("button");
                btn.classList.add("btn","m-2");
                btn.innerHTML = "Izmeni proizvod";
                divIzmeni.appendChild(btn);
                kontejnerIzabranogProizvoda.appendChild(divIzmeni);

                btn.onclick = (ev)=>{

                    for(let i = 0; i < this.cene.length; i++) {
                        this.cene[i] = new Cena(this.cene[i].id, this.cene[i].vrednost, this.cene[i].valuta);
                    }

                    for(let i = 0; i < this.velicinekolicine.length; i++) {
                        this.velicinekolicine[i] = new VelicinaKolicina(this.velicinekolicine[i].id, this.velicinekolicine[i].velicina, this.velicinekolicine[i].kolicina);
                    }

                    for(let i = 0; i < this.modeli.length; i++) {
                        this.modeli[i] = new ModelKolicina(this.modeli[i].id, this.modeli[i].model, this.modeli[i].kolicina);
                    }


                    window.open("izmeni.html?sifraProizvoda="+this.sifraProizvoda, '_blank','noopener noreferrer');
                    localStorage.setItem("proizvod", JSON.stringify({
                        id:this.id,
                        sifraProizvoda:this.sifraProizvoda,
                        naziv:this.naziv,
                        kategorija:this.kategorija,
                        kolicina:this.kolicina,
                        slika:this.slika,
                        cene:this.cene,
                        velicina:this.velicina,
                        opis:this.opis,
                        velicinekolicine:this.velicinekolicine,
                        modeli:this.modeli
                    }));
                }
            }


            if(localStorage.getItem("username") == "admin")
            {
                let divIzmeni = document.createElement("div");
                divIzmeni.classList.add("providno","mx-2");
                let btn = document.createElement("button");
                btn.classList.add("btn","btn-danger","m-2");
                btn.innerHTML = "Obriši proizvod";
                divIzmeni.appendChild(btn);
                kontejnerIzabranogProizvoda.appendChild(divIzmeni);

                btn.onclick = (ev)=>{

                    this.ObrisiProizvod();

                }

            }


    }

    async LajkujOdlajkuj(proizvod) {

        return await this.FetchLajkujOdlajkuj(proizvod);

    }

    async FetchLajkujOdlajkuj(proizvod){

        let username = localStorage.getItem("username");

        if(username === null)
            return false;

        let odgovor = await fetch(this.webapi + "VojniProizvodi/LajkujOdljakuj", {
            method:"POST",
            headers:{
                "Content-Type" : "application/json",
                "Authorization": localStorage.getItem("token")
            },
            body: JSON.stringify({
                korisnickoIme : localStorage.getItem("username"),
                idProizvoda : proizvod.id
            })
        }).catch(reason =>  { 
            return false; 
        });

        if (odgovor == false){
            return false;
        }

        odgovor = await odgovor.text().catch(reason => false);

        if (odgovor == false){
            return false;
        }

        return odgovor;

    }

    async DaLiJeLajkovano(proizvod) {

        return await this.FetchDaLiJeLajkovano(proizvod);

    }

    async FetchDaLiJeLajkovano(proizvod) {

        let username = localStorage.getItem("username");

        if (username === null)
            return false;
        
        let odgovor = await fetch(this.webapi + "VojniProizvodi/DaLiJeLajkovano", {
            method:"POST",
            headers:{
                "Content-Type" : "application/json",
                "Authorization": localStorage.getItem("token")
            },
            body: JSON.stringify({
                korisnickoIme : localStorage.getItem("username"),
                idProizvoda : proizvod.id
            })
        }).catch(reason =>  { 
            return false; 
        });

        if (odgovor == false){
            return false;
        }

        odgovor = await odgovor.text().catch(reason => false);

        if (odgovor == false){
            return false;
        }

        if (odgovor == "Los token") {
            LosToken();
        }

        return odgovor;
    }

    async ObrisiProizvod() {
        
        let odgovor = await fetch(this.webapi + "VojniProizvodi/ObrisiProizvod/" + this.id, {
            method:"POST",
            headers:{
                "Content-Type" : "application/json",
                "Authorization": localStorage.getItem("token")
            }
        }).catch(reason =>  { 
            return false; 
        });

        if (odgovor == false){
            return false;
        }

        odgovor = await odgovor.text().catch(reason => false);

        if (odgovor == false){
            return false;
        }

        if (odgovor == "Uspesno") {
            window.location.reload();
        }

    }

}

// this.id = id;
//         this.sifraProizvoda = sifra;
//         this.naziv = naziv;
//         this.kategorija = kategorija;
//         this.kolicina = kolicina;
//         this.slika = slika;
//         this.cene = cene;
//         this.velicina = velicina;
//         this.opis = opis;
//         this.velicinekolicine = velicinekolicine;
//         this.modeli = modeli;

