
import { Cena } from "./cena.js";
import { VelicinaKolicina } from "./velicinaKolicina.js";
import { ModelKolicina } from "./modelKolicina.js";
import { ObjekatNiz } from "./objekatNiz.js";

const urlParams = new URLSearchParams(window.location.search);

var sifraProizvoda = urlParams.get("sifraProizvoda");


sessionStorage.setItem("webapi","https://localhost:5001/");
var webapi = sessionStorage.getItem("webapi");

//let trenutniProizvod = new Proizvod();
//trenutniProizvod = JSON.parse(localStorage.getItem("proizvod"))

let trenutniProizvod = await VratiProizvod(sifraProizvoda);

var b64encoded = "";

let dugmeIzaberiFajlove = document.querySelector(".IzaberiFajlove");
dugmeIzaberiFajlove.onclick = (ev) => {
    document.querySelector("input[name=Slika]").click();
}

document.querySelector("input[name=Slika]").onchange = (ev) => {

    b64encoded = "";
    document.querySelector(".InvalidPrevelikaSlika").style.display = "none";
    if (document.querySelector("input[name=Slika]").files[0].size/1024/1024 > 7) {
        document.querySelector(".InvalidPrevelikaSlika").style.display = "block";
        return;
    }
    document.querySelector(".OdabranaSlika").querySelector("img").src = "";
    let reader = new FileReader();
    reader.readAsArrayBuffer(document.querySelector("input[name=Slika]").files[0]);
    reader.onloadend = function (evt) {
        if (evt.target.readyState == FileReader.DONE) {
            let arrayBuffer = evt.target.result,
                array = new Uint8Array(arrayBuffer);
                //let decoder = new TextDecoder('ascii');
                //b64encoded = btoa(decoder.decode(array));
                let niz = [];
                var CHUNK_SZ = 0x8000;
                for (let i = 0; i < array.length; i += CHUNK_SZ) {
                    niz.push(String.fromCharCode.apply(null, array.subarray(i,i+CHUNK_SZ)));
                }

                b64encoded = btoa(niz.join(""));
                
                document.querySelector(".OdabranaSlika").querySelector("img").src =  "data:image/*;base64," + b64encoded;
                trenutniProizvod.slika = b64encoded;
        }
    }
    

}

    let kontejner = document.createElement("div");
    kontejner.classList.add("KontejnerProizvoda");

    if (trenutniProizvod.slika != "") {

        let card = document.createElement("div");
        card.classList.add("card-body","bg-white");
                
        let slika = document.createElement("img");
        slika.classList.add("SlikaProizvoda");
        slika.classList.add("card-img-top");
        kontejner.append(card);


        let cardTitle = document.createElement("h5");
        cardTitle.classList.add("card-title");
        cardTitle.innerHTML = trenutniProizvod.naziv; // + " " + this.sifraProizvoda;

        card.append(cardTitle);
        card.style.maxWidth = "100px";
        card.style.maxHeight = "100px";


        slika.src = "data:image/*;base64," + trenutniProizvod.slika;
        kontejner.appendChild(slika);
        let div = document.querySelector(".OdabranaSlika");
        div.insertBefore(kontejner,div.firstChild)

    }

    









let kolicina = document.querySelector(".GrupaKolicina");
let velicina = document.querySelector(".GrupaVelicina");
let velicine = document.querySelector(".VelicineSve");
let modeli = document.querySelector(".ModeliSvi");

if (trenutniProizvod.kategorija == "Odeća") {

    kolicina.style.display = "none";
    velicina.style.display = "none";
    velicine.style.display = "flex";

    modeli.setAttribute('style','display:none !important');
    document.querySelector(".ModelKolicina").innerHTML = "";
    document.querySelector("input[name=Kategorija]").value = trenutniProizvod.kategorija;
    document.querySelector("input[name=Kategorija]").setAttribute("disabled","true");
    document.querySelector("input[name=Naziv]").value = trenutniProizvod.naziv;
    document.querySelector("input[name=Naziv]").setAttribute("disabled","true");
    document.querySelector("textarea[name=Opis]").value = trenutniProizvod.opis;

    let divCene = document.querySelector(".Cene1");
    trenutniProizvod.cene.forEach(el=>{
        let divPom = document.createElement("div");
        divPom.classList.add("d-flex");
        let inputVrednost = document.createElement("input");
        inputVrednost.type = "number";
        inputVrednost.value = el.vrednost;
        divPom.appendChild(inputVrednost);

        let inputValuta = document.createElement("input");
        inputValuta.type = "text";
        inputValuta.value = el.valuta;
        divPom.appendChild(inputValuta);
        
        let x = document.createElement("button");
            
        divPom.appendChild(x);
        x.classList.add("close","btn","CloseMoje");
        x.innerHTML = "&times";
        x.onclick = ev =>{
            divPom.removeChild(inputVrednost);
            divPom.removeChild(inputValuta);
            divPom.removeChild(x);
            divPom.parentElement.removeChild(divPom);
        }

        divCene.append(divPom);
    });
  
    let divVelicineKolicine = document.querySelector(".VelicinaKolicine1");
    trenutniProizvod.velicineKolicine.forEach(el=>{
        let divPom = document.createElement("div");
        divPom.classList.add("d-flex");
        let inputVelicina = document.createElement("input");
        inputVelicina.type = "text";
        inputVelicina.value = el.velicina;
        divPom.appendChild(inputVelicina);

        let inputKolicina = document.createElement("input");
        inputKolicina.type = "number";
        inputKolicina.value = el.kolicina;
        divPom.appendChild(inputKolicina);
        
        let x = document.createElement("button");
            
            divPom.appendChild(x);
            x.classList.add("close","btn","CloseMoje");
            x.innerHTML = "&times";
            x.onclick = ev =>{
                divPom.removeChild(inputVelicina);
                divPom.removeChild(inputKolicina);
                divPom.removeChild(x);
                divPom.parentElement.removeChild(divPom);
            }

        divVelicineKolicine.append(divPom);
    });
    
}
else if (trenutniProizvod.kategorija == "Obuća") {

    kolicina.style.display = "none";
    velicina.style.display = "none";
    velicine.style.display = "flex";
    modeli.setAttribute('style','display:none !important');
    document.querySelector(".ModelKolicina").innerHTML = "";
  
    document.querySelector("input[name=Kategorija]").value = trenutniProizvod.kategorija;
    document.querySelector("input[name=Kategorija]").setAttribute("disabled","true");
    document.querySelector("input[name=Naziv]").value = trenutniProizvod.naziv;
    document.querySelector("input[name=Naziv]").setAttribute("disabled","true");
    document.querySelector("textarea[name=Opis]").value = trenutniProizvod.opis;
  
    let divCene = document.querySelector(".Cene1");
    trenutniProizvod.cene.forEach(el=>{
        let divPom = document.createElement("div");
        divPom.classList.add("d-flex");
        let inputVrednost = document.createElement("input");
        inputVrednost.type = "number";
        inputVrednost.value = el.vrednost;
        divPom.appendChild(inputVrednost);

        let inputValuta = document.createElement("input");
        inputValuta.type = "text";
        inputValuta.value = el.valuta;
        divPom.appendChild(inputValuta);
        
        let x = document.createElement("button");
            
        divPom.appendChild(x);
        x.classList.add("close","btn","CloseMoje");
        x.innerHTML = "&times";
        x.onclick = ev =>{
            divPom.removeChild(inputVrednost);
            divPom.removeChild(inputValuta);
            divPom.removeChild(x);
            divPom.parentElement.removeChild(divPom);
        }

        divCene.append(divPom);
    });

    let divVelicineKolicine = document.querySelector(".VelicinaKolicine1");
    trenutniProizvod.velicineKolicine.forEach(el=>{
        let divPom = document.createElement("div");
        divPom.classList.add("d-flex");
        let inputVelicina = document.createElement("input");
        inputVelicina.type = "text";
        inputVelicina.value = el.velicina;
        divPom.appendChild(inputVelicina);

        let inputKolicina = document.createElement("input");
        inputKolicina.type = "number";
        inputKolicina.value = el.kolicina;
        divPom.appendChild(inputKolicina);
        
        let x = document.createElement("button");
            
            divPom.appendChild(x);
            x.classList.add("close","btn","CloseMoje");
            x.innerHTML = "&times";
            x.onclick = ev =>{
                divPom.removeChild(inputVelicina);
                divPom.removeChild(inputKolicina);
                divPom.removeChild(x);
                divPom.parentElement.removeChild(divPom);
            }

        divVelicineKolicine.append(divPom);
    });


}
else if (trenutniProizvod.kategorija == "Oprema") {

    kolicina.style.display = "block";
    velicina.style.display = "block";
    velicine.setAttribute('style','display:none !important');
    document.querySelector(".VelicinaKolicine").innerHTML = "";
    modeli.style.display = "flex";


   document.querySelector("input[name=Kategorija]").value = trenutniProizvod.kategorija;
   document.querySelector("input[name=Kategorija]").setAttribute("disabled","true");

   document.querySelector("input[name=Naziv]").value = trenutniProizvod.naziv;
   document.querySelector("input[name=Naziv]").setAttribute("disabled","true");

   document.querySelector("textarea[name=Opis]").value = trenutniProizvod.opis;
   document.querySelector("input[name=Kolicina]").value = trenutniProizvod.kolicina;
   document.querySelector("input[name=Velicina]").value = trenutniProizvod.velicina;

   
    let divCene = document.querySelector(".Cene1");
    trenutniProizvod.cene.forEach(el=>{
        let divPom = document.createElement("div");
        divPom.classList.add("d-flex");
        let inputVrednost = document.createElement("input");
        inputVrednost.type = "number";
        inputVrednost.value = el.vrednost;
        divPom.appendChild(inputVrednost);

        let inputValuta = document.createElement("input");
        inputValuta.type = "text";
        inputValuta.value = el.valuta;
        divPom.appendChild(inputValuta);
        
        let x = document.createElement("button");
            
        divPom.appendChild(x);
        x.classList.add("close","btn","CloseMoje");
        x.innerHTML = "&times";
        x.onclick = ev =>{
            divPom.removeChild(inputVrednost);
            divPom.removeChild(inputValuta);
            divPom.removeChild(x);
            divPom.parentElement.removeChild(divPom);
        }

        divCene.append(divPom);
    });

    let divVelicineKolicine = document.querySelector(".VelicinaKolicine1");
    trenutniProizvod.velicineKolicine.forEach(el=>{
        let divPom = document.createElement("div");
        divPom.classList.add("d-flex");
        let inputVelicina = document.createElement("input");
        inputVelicina.type = "text";
        inputVelicina.value = el.velicina;
        divPom.appendChild(inputVelicina);

        let inputKolicina = document.createElement("input");
        inputKolicina.type = "number";
        inputKolicina.value = el.kolicina;
        divPom.appendChild(inputKolicina);
        
        let x = document.createElement("button");
            
            divPom.appendChild(x);
            x.classList.add("close","btn","CloseMoje");
            x.innerHTML = "&times";
            x.onclick = ev =>{
                divPom.removeChild(inputVelicina);
                divPom.removeChild(inputKolicina);
                divPom.removeChild(x);
                divPom.parentElement.removeChild(divPom);
            }

        divVelicineKolicine.append(divPom);
    });

    let divModelKolicina = document.querySelector(".ModelKolicina1");
    trenutniProizvod.modeli.forEach(el=>{
        let divPom = document.createElement("div");
        divPom.classList.add("d-flex");
        let inputModel = document.createElement("input");
        inputModel.type = "text";
        inputModel.value = el.model;
        divPom.appendChild(inputModel);

        let inputKolicina = document.createElement("input");
        inputKolicina.type = "number";
        inputKolicina.value = el.kolicina;
        divPom.appendChild(inputKolicina);
        
        let x = document.createElement("button");
            
            divPom.appendChild(x);
            x.classList.add("close","btn","CloseMoje");
            x.innerHTML = "&times";
            x.onclick = ev =>{
                divPom.removeChild(inputModel);
                divPom.removeChild(inputKolicina);
                divPom.removeChild(x);
                divPom.parentElement.removeChild(divPom);
            }
            
        divModelKolicina.append(divPom);
    });

}
else {

    kolicina.style.display = "block";
    velicina.style.display = "block";
    velicine.setAttribute('style','display:none !important');
    document.querySelector(".VelicinaKolicine").innerHTML = "";
    modeli.style.display = "flex";


   document.querySelector("input[name=Kategorija]").value = trenutniProizvod.kategorija;
   document.querySelector("input[name=Kategorija]").setAttribute("disabled","true");

   document.querySelector("input[name=Naziv]").value = trenutniProizvod.naziv;
   document.querySelector("input[name=Naziv]").setAttribute("disabled","true");

   document.querySelector("textarea[name=Opis]").value = trenutniProizvod.opis;
   document.querySelector("input[name=Kolicina]").value = trenutniProizvod.kolicina;
   document.querySelector("input[name=Velicina]").value = trenutniProizvod.velicina;


   let divCene = document.querySelector(".Cene1");
    trenutniProizvod.cene.forEach(el=>{
        let divPom = document.createElement("div");
        divPom.classList.add("d-flex");
        let inputVrednost = document.createElement("input");
        inputVrednost.type = "number";
        inputVrednost.value = el.vrednost;
        divPom.appendChild(inputVrednost);

        let inputValuta = document.createElement("input");
        inputValuta.type = "text";
        inputValuta.value = el.valuta;
        divPom.appendChild(inputValuta);
        
        let x = document.createElement("button");
            
        divPom.appendChild(x);
        x.classList.add("close","btn","CloseMoje");
        x.innerHTML = "&times";
        x.onclick = ev =>{
            divPom.removeChild(inputVrednost);
            divPom.removeChild(inputValuta);
            divPom.removeChild(x);
            divPom.parentElement.removeChild(divPom);
        }

        divCene.append(divPom);
    });

    let divModelKolicina = document.querySelector(".ModelKolicina1");
    trenutniProizvod.modeli.forEach(el=>{
        let divPom = document.createElement("div");
        divPom.classList.add("d-flex");
        let inputModel = document.createElement("input");
        inputModel.type = "text";
        inputModel.value = el.model;
        divPom.appendChild(inputModel);

        let inputKolicina = document.createElement("input");
        inputKolicina.type = "number";
        inputKolicina.value = el.kolicina;
        divPom.appendChild(inputKolicina);
        
        let x = document.createElement("button");
            
            divPom.appendChild(x);
            x.classList.add("close","btn","CloseMoje");
            x.innerHTML = "&times";
            x.onclick = ev =>{
                divPom.removeChild(inputModel);
                divPom.removeChild(inputKolicina);
                divPom.removeChild(x);
                divPom.parentElement.removeChild(divPom);
            }
            
        divModelKolicina.append(divPom);
    });

}

var ceneProizvoda = new ObjekatNiz();
var velicineProizvoda = new ObjekatNiz();
var modeliProizvoda = new ObjekatNiz();


document.querySelector(".DodajCenu").onclick = (ev) => {

    let vr = parseFloat(document.querySelector(".RedCene").querySelector("input[name=Vrednost]").value);
    let val = document.querySelector(".RedCene").querySelector("input[name=Valuta]").value;

    if ((!isNaN(vr) || vr == -1)  && val != "") {
        if (vr == -1) {
            vr = null;
        }
        else {
            vr = Math.abs(vr);
        }
        
        
        ceneProizvoda.niz.push(new Cena(null, vr, val));
        ceneProizvoda.niz[ceneProizvoda.niz.length - 1].Prikaz(document.querySelector(".Cene"), ceneProizvoda, ceneProizvoda.niz.length - 1);
    }

}

document.querySelector(".DodajVelicinuKolicinu").onclick = (ev) => {

    let vel = document.querySelector(".RedVelicinaKolicina").querySelector("input[name=Velicina]").value;
    let kol = parseInt(document.querySelector(".RedVelicinaKolicina").querySelector("input[name=Kolicina]").value);

    if (!isNaN(kol) && vel != "") {
        kol = Math.abs(kol);

        velicineProizvoda.niz.push(new VelicinaKolicina(null, vel, kol));
        velicineProizvoda.niz[velicineProizvoda.niz.length - 1].Prikaz(document.querySelector(".VelicinaKolicine"), velicineProizvoda, velicineProizvoda.niz.length - 1);
    }

}

document.querySelector(".DodajModelKolicinu").onclick = (ev) => {

    let vel = document.querySelector(".RedModelKolicina").querySelector("input[name=Model]").value;
    let kol = parseInt(document.querySelector(".RedModelKolicina").querySelector("input[name=Kolicina]").value);

    if (!isNaN(kol) && vel != "") {
        kol = Math.abs(kol);

        modeliProizvoda.niz.push(new ModelKolicina(null, vel, kol));
        modeliProizvoda.niz[modeliProizvoda.niz.length - 1].Prikaz(document.querySelector(".ModelKolicina"), modeliProizvoda, modeliProizvoda.niz.length - 1);
    }

    

}




let btnIzmeniProizvod = document.querySelector(".IzmeniProizvodDugme");

btnIzmeniProizvod.onclick = ev => {

    trenutniProizvod.cene = [];

    let sve = document.querySelector(".Cene1").querySelectorAll("div");
    sve.forEach(d => {

        let vr = parseFloat(d.querySelector("input").value);
        let val = d.querySelectorAll("input")[1].value;

        if ((!isNaN(vr) || vr == -1)  && val != "") {
            if (vr == -1) {
                vr = null;
            }
            else {
                vr = Math.abs(vr);
            }
            
            
            trenutniProizvod.cene.push(new Cena(null, vr, val));
        }

    })

    trenutniProizvod.velicinekolicine = [];

    sve = document.querySelector(".VelicinaKolicine1").querySelectorAll("div");
    sve.forEach(d => {

        let vel = d.querySelector("input").value;
        let kol = parseInt(d.querySelectorAll("input")[1].value);

        if (!isNaN(kol) && vel != "") {
            kol = Math.abs(kol);
    
            trenutniProizvod.velicinekolicine.push(new VelicinaKolicina(null, vel, kol));
        }
        
    })

    trenutniProizvod.modeli = [];

    sve = document.querySelector(".ModelKolicina1").querySelectorAll("div");
    sve.forEach(d => {

        let vel = d.querySelector("input").value;
        let kol = parseInt(d.querySelectorAll("input")[1].value);

        if (!isNaN(kol) && vel != "") {
            kol = Math.abs(kol);
    
            trenutniProizvod.modeli.push(new ModelKolicina(null, vel, kol));
        }
        
    })



    if (trenutniProizvod.kategorija == "Odeća") {
        trenutniProizvod.opis = document.querySelector("textarea[name=Opis]").value;
    }
    else if (trenutniProizvod.kategorija == "Obuća"){
        trenutniProizvod.opis = document.querySelector("textarea[name=Opis]").value;
    }
    else if(trenutniProizvod.kategorija == "Oprema"){
        trenutniProizvod.opis = document.querySelector("textarea[name=Opis]").value;
        trenutniProizvod.kolicina = document.querySelector("input[name=Kolicina]").value;
        trenutniProizvod.velicina = document.querySelector("input[name=Velicina]").value;
    }

    let kolicina = parseInt(document.querySelector(".GrupaKolicina").querySelector("input").value);
    let velicina = document.querySelector(".GrupaVelicina").querySelector("input").value;
    let opis = document.querySelector("textarea[name=Opis]").value;
    let fleg = true;

    let naziv = document.querySelector("input[name=Naziv]").value;
    if (naziv == "") {
        document.querySelector(".NazivInvalid").style.display = "block";
        fleg = false;
    }

    if (ceneProizvoda.niz.length + trenutniProizvod.cene.length == 0) {
        document.querySelector(".CenaInvalid").style.display = "block";
        fleg = false;
    }

    if (trenutniProizvod.kategorija == "Odeća" || trenutniProizvod.kategorija == "Obuća") {
        if (velicineProizvoda.niz.length + trenutniProizvod.velicinekolicine.length == 0) {
            document.querySelector(".VelicineKolicineInvalid").style.display = "block";
            fleg = false;
        }
        kolicina = 0;
        velicineProizvoda.niz.forEach(n => {
            kolicina = kolicina + n.kolicina;
        })
        trenutniProizvod.velicinekolicine.forEach(n => {
            kolicina = kolicina + n.kolicina;
        })
    }
    else {
        if (isNaN(kolicina)) {
            document.querySelector(".KolicinaInvalid").style.display = "block";
            fleg = false;
        }
    }

    if (fleg == false) {
        return;
    }

    trenutniProizvod.kolicina = kolicina;

    IzmeniProizvod();

}
async function IzmeniProizvod() {

    ceneProizvoda.niz.forEach(c => {
        trenutniProizvod.cene.push(new Cena(null, c.vrednost, c.valuta));
    })

    velicineProizvoda.niz.forEach(c => {
        trenutniProizvod.velicinekolicine.push(new VelicinaKolicina(null, c.velicina, c.kolicina));
    })

    modeliProizvoda.niz.forEach(c => {
        trenutniProizvod.modeli.push(new ModelKolicina(null, c.model, c.kolicina));
    })

    FetchIzmeniProizvod();

}

async function FetchIzmeniProizvod() {
    


    let odgovor = await fetch(webapi + "VojniProizvodi/IzmeniProizvod", {
        method: "PUT",
        headers:{
            "Content-Type" : "application/json",
            "Authorization" : localStorage.getItem("token")
        },
        body: JSON.stringify({
            id: trenutniProizvod.id,
            sifraProizvoda : trenutniProizvod.sifraProizvoda,
            naziv : trenutniProizvod.naziv,
            kategorija : trenutniProizvod.kategorija,
            kolicina : trenutniProizvod.kolicina,
            cene:trenutniProizvod.cene,
            slika:trenutniProizvod.slika,
            velicina:trenutniProizvod.velicina,
            opis:trenutniProizvod.opis,
            velicineKolicine:trenutniProizvod.velicinekolicine,
            modeli:trenutniProizvod.modeli
        })
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.text().catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    if(odgovor == "Los token"){
        LosToken();
    }
    else if (odgovor == "Ukupna kolicina ne moze biti manja od zbira kolicina pojedinacnih velicina") {
        document.querySelector(".KolicineInvalid").style.display = "block";
    }
    else if (odgovor == "Ukupna kolicina ne moze biti manja od zbira kolicina pojedinacnih modela") {
        document.querySelector(".KolicineModInvalid").style.display = "block";
    }
    else if (odgovor == "Uspesno") {
        document.querySelector(".Uspesno").style.display = "block";
    }

}

function LosToken() {

    let pozadina = document.createElement("div");
    pozadina.classList.add("PozadinaZaGreskuPrijavljivanja", "text-light");
    pozadina.innerHTML = "Potrebno je ponovno prijavljivanje!";
    document.body.appendChild(pozadina);

        let div = document.createElement("div");
        pozadina.appendChild(div);

        let dugmePrijaviSeOpet = document.createElement("button");
        dugmePrijaviSeOpet.classList.add("btn", "text-light", "m-5");
        dugmePrijaviSeOpet.innerHTML = 'Prijavljivanje';
        div.appendChild(dugmePrijaviSeOpet);

        dugmePrijaviSeOpet.onclick = (ev) => {

            localStorage.removeItem("token");
            localStorage.removeItem("username");
            
            window.location.href = "index.html";

        }
}

async function VratiProizvod(sifra){
     return FetchVratiProizvod(sifra);
}

async function FetchVratiProizvod(sifra){

    let odgovor = await fetch(webapi + "VojniProizvodi/VratiProizvod/"+sifra, {
        method: 'GET'
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.json().catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    return odgovor;
}
