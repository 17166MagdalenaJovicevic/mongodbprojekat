export class VelicinaKolicina {

    constructor(id, velicina, kolicina) {
        this.id = id;
        this.velicina = velicina;
        this.kolicina = kolicina;
    }

    Prikaz(host, niz, br) {

        let div = document.createElement("div");
        div.classList.add("d-flex","flex-row","justify-content-between");

        let div2 = document.createElement("div");
        div2.innerHTML = this.velicina + " " + this.kolicina;
        div.appendChild(div2);

        let x = document.createElement("button");
        x.classList.add("close","btn","CloseMoje");
        x.innerHTML = "&times";
        x.onclick = (ev) => {
            niz.niz = niz.niz.filter((n,i) => n != br);
            x.parentElement.parentElement.removeChild(x.parentElement);
        }

        div.appendChild(x);

        host.appendChild(div);

    }

    JsonStringifyVelicinaKolicina() {
        return {
            velicina : this.velicina,
            kolicina: this.kolicina
        }
    }
    
}