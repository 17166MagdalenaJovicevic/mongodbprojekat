import { Cena } from "./cena.js";
import { ModelKolicina } from "./modelKolicina.js";
import { VelicinaKolicina } from "./velicinaKolicina.js";
import { ObjekatNiz } from "./objekatNiz.js";

sessionStorage.setItem("webapi","https://localhost:5001/");
var webapi = sessionStorage.getItem("webapi");

var ceneNovogProizvoda = new ObjekatNiz();
var velicineNovogProizvoda = new ObjekatNiz();
var modeliNovogProizvoda = new ObjekatNiz();
var b64encoded = "";

let dugmeIzaberiFajlove = document.querySelector(".IzaberiFajlove");
dugmeIzaberiFajlove.onclick = (ev) => {
    document.querySelector("input[name=Slika]").click();
}

document.querySelector("input[name=Slika]").onchange = (ev) => {
    b64encoded = "";
    document.querySelector(".InvalidPrevelikaSlika").style.display = "none";
    if (document.querySelector("input[name=Slika]").files[0].size/1024/1024 > 7) {
        document.querySelector(".InvalidPrevelikaSlika").style.display = "block";
        return;
    }
    document.querySelector(".OdabranaSlika").querySelector("img").src = "";
    let reader = new FileReader();
    reader.readAsArrayBuffer(document.querySelector("input[name=Slika]").files[0]);
    reader.onloadend = function (evt) {
        if (evt.target.readyState == FileReader.DONE) {
            let arrayBuffer = evt.target.result,
                array = new Uint8Array(arrayBuffer);
                //let decoder = new TextDecoder('ascii');
                //b64encoded = btoa(decoder.decode(array));
                let niz = [];
                var CHUNK_SZ = 0x8000;
                for (let i = 0; i < array.length; i += CHUNK_SZ) {
                    niz.push(String.fromCharCode.apply(null, array.subarray(i,i+CHUNK_SZ)));
                }

                b64encoded = btoa(niz.join(""));
                
                document.querySelector(".OdabranaSlika").querySelector("img").src =  "data:image/*;base64," + b64encoded;
        }
    }
    

}

var nizKategorija = [];
var selectKategoriju = document.querySelector("select[name=Kategorija]");

Inicijalizacija();

async function Inicijalizacija() {
    
    nizKategorija = await VratiKategorijeProizvoda();
    if (nizKategorija != false) {
        nizKategorija.forEach(k => {
            let opt = document.createElement("option");
            opt.classList.add("OpcijaKategorija");
            opt.value = k.text;
            opt.innerHTML = k.text;
            selectKategoriju.appendChild(opt);
        })
    }
    let opt = document.createElement("option");
    opt.classList.add("OpcijaKategorija","btn");
    opt.value = "null";
    opt.innerHTML = "Dodaj novi tip +";
    selectKategoriju.appendChild(opt);
    selectKategoriju.querySelector("option[value=null]").selected = true;

}

function LosToken() {

    let pozadina = document.createElement("div");
    pozadina.classList.add("PozadinaZaGreskuPrijavljivanja", "text-light");
    pozadina.innerHTML = "Potrebno je ponovno prijavljivanje!";
    document.body.appendChild(pozadina);

        let div = document.createElement("div");
        pozadina.appendChild(div);

        let dugmePrijaviSeOpet = document.createElement("button");
        dugmePrijaviSeOpet.classList.add("btn", "text-light", "m-5");
        dugmePrijaviSeOpet.innerHTML = 'Prijavljivanje';
        div.appendChild(dugmePrijaviSeOpet);

        dugmePrijaviSeOpet.onclick = (ev) => {

            localStorage.removeItem("token");
            localStorage.removeItem("username");
            
            window.location.href = "index.html";

        }
}

selectKategoriju.onchange = (ev) => {
    document.querySelectorAll(".invalid-feedback").forEach(i => {
        i.style.display = "none";
    })
    if (selectKategoriju.value == "null") {
        document.querySelector(".NoviTipProizvoda").style.display = "block";
    }
    else {
        document.querySelector(".NoviTipProizvoda").style.display = "none";
    }
    let kolicina = document.querySelector(".GrupaKolicina");
    let velicina = document.querySelector(".GrupaVelicina");
    let velicine = document.querySelector(".VelicineSve");
    let modeli = document.querySelector(".ModeliSvi");
    if (selectKategoriju.value == "Odeća") {
        kolicina.style.display = "none";
        velicina.style.display = "none";
        velicine.style.display = "flex";
        modeli.setAttribute('style','display:none !important');
        document.querySelector(".ModelKolicina").innerHTML = "";
        modeliNovogProizvoda = new ObjekatNiz();
    }
    else if (selectKategoriju.value == "Obuća") {
        kolicina.style.display = "none";
        velicina.style.display = "none";
        velicine.style.display = "flex";
        modeli.setAttribute('style','display:none !important');
        document.querySelector(".ModelKolicina").innerHTML = "";
        modeliNovogProizvoda = new ObjekatNiz();
    }
    else if (selectKategoriju.value == "Oprema") {
        kolicina.style.display = "block";
        velicina.style.display = "block";
        velicine.setAttribute('style','display:none !important');
        document.querySelector(".VelicinaKolicine").innerHTML = "";
        modeli.style.display = "flex";
        velicineNovogProizvoda = new ObjekatNiz();
    }
    else {
        kolicina.style.display = "block";
        velicina.style.display = "block";
        velicine.setAttribute('style','display:none !important');
        document.querySelector(".VelicinaKolicine").innerHTML = "";
        modeli.style.display = "flex";
        velicineNovogProizvoda = new ObjekatNiz();
    }
}


async function VratiKategorijeProizvoda() {

    let odgovor = await fetch(webapi + "VojniProizvodi/VratiKategorije", {
        method: 'GET'
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.json().catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    return odgovor;

}


document.querySelector(".DodajCenu").onclick = (ev) => {

    let vr = parseFloat(document.querySelector(".RedCene").querySelector("input[name=Vrednost]").value);
    let val = document.querySelector(".RedCene").querySelector("input[name=Valuta]").value;

    if ((!isNaN(vr) || vr == -1)  && val != "") {
        if (vr == -1) {
            vr = null;
        }
        else {
            vr = Math.abs(vr);
        }
        ceneNovogProizvoda.niz.push(new Cena(null, vr, val));
        ceneNovogProizvoda.niz[ceneNovogProizvoda.niz.length - 1].Prikaz(document.querySelector(".Cene"), ceneNovogProizvoda, ceneNovogProizvoda.niz[ceneNovogProizvoda.niz.length - 1]);
    }

}

document.querySelector(".DodajVelicinuKolicinu").onclick = (ev) => {

    let vel = document.querySelector(".RedVelicinaKolicina").querySelector("input[name=Velicina]").value;
    let kol = parseInt(document.querySelector(".RedVelicinaKolicina").querySelector("input[name=Kolicina]").value);

    if (!isNaN(kol) && vel != "") {
        kol = Math.abs(kol);
        velicineNovogProizvoda.niz.push(new VelicinaKolicina(null, vel, kol));
        velicineNovogProizvoda.niz[velicineNovogProizvoda.niz.length - 1].Prikaz(document.querySelector(".VelicinaKolicine"), velicineNovogProizvoda, velicineNovogProizvoda.niz[velicineNovogProizvoda.niz.length - 1]);
    }

}

document.querySelector(".DodajModelKolicinu").onclick = (ev) => {

    let vel = document.querySelector(".RedModelKolicina").querySelector("input[name=Model]").value;
    let kol = parseInt(document.querySelector(".RedModelKolicina").querySelector("input[name=Kolicina]").value);

    if (!isNaN(kol) && vel != "") {
        kol = Math.abs(kol);
        modeliNovogProizvoda.niz.push(new ModelKolicina(null, vel, kol));
        modeliNovogProizvoda.niz[modeliNovogProizvoda.niz.length - 1].Prikaz(document.querySelector(".ModelKolicina"), modeliNovogProizvoda, modeliNovogProizvoda.niz[modeliNovogProizvoda.niz.length - 1]);
    }

}


document.querySelector(".DodajProizvodDugme").onclick = (ev) => {

    document.querySelector(".Uspesno").style.display = "none";

    let fleg = true;

    document.querySelectorAll(".invalid-feedback").forEach(i => {
        i.style.display = "none";
    })

    let noviTip = "false";

    let kategorija = selectKategoriju.value;
    if (kategorija == "null") {
        noviTip = document.querySelector("input[name=NoviTip]").value;
        if (noviTip == "" || noviTip == "null") {
            document.querySelector(".NoviTipInvalid").style.display = "block";
            fleg = false;
        }
        else {
            kategorija = noviTip;
            noviTip = "true";
        }
    }


    let kolicina = parseInt(document.querySelector(".GrupaKolicina").querySelector("input").value);
    let velicina = document.querySelector(".GrupaVelicina").querySelector("input").value;
    let opis = document.querySelector("textarea[name=Opis]").value;

    let naziv = document.querySelector("input[name=Naziv]").value;
    if (naziv == "") {
        document.querySelector(".NazivInvalid").style.display = "block";
        fleg = false;
    }

    if (ceneNovogProizvoda.niz.length == 0) {
        document.querySelector(".CenaInvalid").style.display = "block";
        fleg = false;
    }

    if (selectKategoriju.value == "Odeća" || selectKategoriju.value == "Obuća") {
        if (velicineNovogProizvoda.niz.length == 0) {
            document.querySelector(".VelicineKolicineInvalid").style.display = "block";
            fleg = false;
        }
        kolicina = 0;
        velicineNovogProizvoda.niz.forEach(n => {
            kolicina = kolicina + n.kolicina;
        })
    }
    else {
        if (isNaN(kolicina)) {
            document.querySelector(".KolicinaInvalid").style.display = "block";
            fleg = false;
        }
    }

    if (fleg == false) {
        return;
    }

    let slika = b64encoded;

    DodajProizvod(kategorija, noviTip, naziv, kolicina, ceneNovogProizvoda.niz, slika, velicina, velicineNovogProizvoda.niz, modeliNovogProizvoda.niz, opis)

}

async function DodajProizvod(kategorija, noviTip, naziv, kolicina, ceneNovogProizvoda, slika, velicina, velicineNovogProizvoda, modeliNovogProizvoda, opis) {

    let cene = [];
    for(let i = 0; i < ceneNovogProizvoda.length; i++) {
        cene.push(ceneNovogProizvoda[i].JsonStringifyCena());
    }
        
    let velicineKolicine = [];
    for(let i = 0; i < velicineNovogProizvoda.length; i++) {
        velicineKolicine.push(velicineNovogProizvoda[i].JsonStringifyVelicinaKolicina());
    }

    let modeli = [];
    for(let i = 0; i < modeliNovogProizvoda.length; i++) {
        modeli.push(modeliNovogProizvoda[i].JsonStringifyModelKolicina());
    }

    let odgovor = await fetch(webapi+"VojniProizvodi/DodajProizvod/"+ noviTip, {
        method:"POST",
        headers:{
            "Content-Type" : "application/json",
            'Authorization' : localStorage.getItem("token")
        },
        body: JSON.stringify({
            naziv: naziv,
            kategorija: kategorija,
            kolicina: kolicina,
            cene: cene,
            slika: slika,
            opis: opis,
            velicina: velicina,
            velicineKolicine: velicineKolicine,
            modeli: modeli
        })
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        return false;
    }

    odgovor = await odgovor.text().catch(reason => false);

    if (odgovor == false){
        return false;
    }

    if (odgovor == "Los token") {
        LosToken();
        return;
    }

    if (odgovor == "Ukupna kolicina ne moze biti manja od zbira kolicina pojedinacnih velicina") {
        document.querySelector(".KolicineInvalid").style.display = "block";
    }
    else if (odgovor == "Ukupna kolicina ne moze biti manja od zbira kolicina pojedinacnih modela") {
        document.querySelector(".KolicineModInvalid").style.display = "block";
    }
    else {
        document.querySelector(".Uspesno").style.display = "block";
        document.querySelector(".SifraDodatogProizvoda").innerHTML = "Šifra prethodno dodatog proizvoda je: " + odgovor;
    }

}