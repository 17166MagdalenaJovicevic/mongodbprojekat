export class Cena {

    constructor(id, vrednost, valuta) {
        this.id = id;
        this.vrednost = vrednost;
        this.valuta = valuta;
    }

    Prikaz(host, niz, br) {

        let div = document.createElement("div");
        div.classList.add("d-flex","flex-row","justify-content-between");

        let div2 = document.createElement("div");
        div2.innerHTML = this.vrednost + " " + this.valuta;
        div.appendChild(div2);

        let x = document.createElement("button");
        x.classList.add("close","btn","CloseMoje");
        x.innerHTML = "&times";
        x.onclick = (ev) => {
            niz.niz = niz.niz.filter((n,i) => n != br);
            x.parentElement.parentElement.removeChild(x.parentElement);
        }

        div.appendChild(x);

        host.appendChild(div);

    }

    JsonStringifyCena() {
        return {
            vrednost : this.vrednost,
            valuta: this.valuta
        }
    }

}