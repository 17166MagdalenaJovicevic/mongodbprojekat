import {Proizvod} from "./proizvod.js"
sessionStorage.setItem("webapi","https://localhost:5001/");
var webapi = sessionStorage.getItem("webapi");

var KontejnerProizvoda = document.querySelector(".Proizvodi");
var KontejnerDugmicaStranica = document.querySelector(".container-Proizvodi").querySelector(".DugmiciStranica");
let DugmePretrazi = document.querySelector(".DugmePretrazi");
let InputPretrazi = document.querySelector(".InputPretrazi");
let DugmeOmiljeno = document.querySelector(".Omiljeno");

InputPretrazi.onkeypress = (ev) => {
    if (ev.keyCode === 13) {
        DugmePretrazi.click();
    }
}


Inicijalizacija();


// 


async function Inicijalizacija() {

    if (localStorage.getItem("username") != null) {

        document.querySelector(".PrijaviSe").style.display = "none";
        document.querySelector(".FormaZaPrijavljivanje").setAttribute('style','display: none !important');
        document.querySelector(".PrijavljeniKorisnik").innerHTML = localStorage.getItem("username");
        document.querySelector(".OdjaviSe").style.display = "block";
        document.querySelector(".PromeniSifru").style.display = "block";
        document.querySelector(".Omiljeno").style.display = "block";
        document.querySelector(".RegistrujSe").style.display = "none";
        document.querySelector(".PrikazOdabranog").childNodes.forEach(c => {
            if (c.nodeType == 1)
                c.setAttribute('style','display: none !important');
        })

    }

    await VratiKategorije();

    GenerisiStranicePretrage();

}


export function LosToken() {

    OdjaviSe();

}

async function VratiKategorije() {

    let odgovor = await fetch(webapi + "VojniProizvodi/VratiKategorije", {
        method: 'GET'
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.json().catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    let kontejner = document.querySelector(".Filteri");

    odgovor.forEach(k => {

        let div = document.createElement("div");
        div.classList.add("form-check","ml-1");

        let input = document.createElement("input");
        input.classList.add("form-check-input");
        input.type = "checkbox";
        input.name = "cbx" + k.text;
        input.value = k.text;
        input.checked = true;

        let label = document.createElement("label");
        label.classList.add("form-check-label");
        label.innerHTML = k.text;

        div.appendChild(input);
        div.appendChild(label);

        kontejner.appendChild(div);

    })

}

DugmePretrazi.onclick = () => {
    
    GenerisiStranicePretrage()

}

async function GenerisiStranicePretrage() {


    //Izbiri postojece dugmice
    while(KontejnerDugmicaStranica.firstChild)
        KontejnerDugmicaStranica.removeChild(KontejnerDugmicaStranica.firstChild);

    let brojStranica = await VratiBrojProizvoda();
    if (brojStranica % 6 == 0) {
        brojStranica--;
    }

    brojStranica = brojStranica / 6;
    brojStranica = Math.floor(brojStranica);

    for(let i=0; i <= brojStranica; i++){
        let dugmeStranice = document.createElement("button");
        dugmeStranice.innerHTML = i+1;
        dugmeStranice.classList.add("btn","DugmeStranice");
        dugmeStranice.classList.add("btn-outline-secondary");
        dugmeStranice.onclick = (ev) => {

            document.querySelectorAll(".DugmeStranice").forEach(d => {
                d.classList.remove("bg-dark","text-light");
            })

            dugmeStranice.classList.add("bg-dark","text-light");

            // Brise postojeci sadrzaj na stanici
            while(KontejnerProizvoda.firstChild)
            KontejnerProizvoda.removeChild(KontejnerProizvoda.firstChild);

            Vrati6PretrazeneProizvode((i)*6);
        }
        KontejnerDugmicaStranica.appendChild(dugmeStranice);
        if(i === 0)
            dugmeStranice.click();

    }

}

async function Vrati6PretrazeneProizvode(polozaj) {

    let listaProizvoda = await FetchVrati6PretrazeneProizvode(polozaj);
    if (listaProizvoda == null || listaProizvoda == false) {
        return false;
    }

    let kontejnerProizvoda = document.querySelector(".Proizvodi");
    kontejnerProizvoda.innerHTML = "";

    listaProizvoda.forEach(proizvod => {
        proizvod.PrikazProizvoda(kontejnerProizvoda);
    })

}

async function FetchVrati6PretrazeneProizvode(polozaj) {

    let tekstPretrage = InputPretrazi.value.trim();
    let fleg = "true";
    if (tekstPretrage == "") {
        tekstPretrage = "bla";
        fleg = "false";
    }

    let sortira = document.querySelector("select[name=Sortiraj]").value;

    let kategorije = [];

    let checkboxes = document.querySelector(".Filteri").querySelectorAll("input");
    checkboxes.forEach(cbx => {
        if (cbx.checked == true) {
            kategorije.push(cbx.value);
        }
    })
    if (kategorije.length == 0) {
        checkboxes.forEach(cbx => {
            kategorije.push(cbx.value);
        })
    }

    let odgovor = await fetch(webapi + "VojniProizvodi/Vrati6PretrazeneProizvode/" + tekstPretrage + "/" + polozaj + "/" + sortira + "/" + fleg, {
        method: 'POST',
        headers: {
            "Content-Type" : "application/json"
        },
        body: JSON.stringify(kategorije)
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaProizvoda  = [];

    odgovor.forEach(proizvod => {
        listaProizvoda.push(new Proizvod(proizvod.id, proizvod.sifraProizvoda, proizvod.naziv, proizvod.kategorija,
             proizvod.kolicina, proizvod.cene, proizvod.slika, proizvod.velicina, proizvod.opis,
            proizvod.velicineKolicine, proizvod.modeli));
    })

    return listaProizvoda;

}



async function GenerisiStraniceOmiljenih() {
    
    let KontejnerDugmicaStranica = document.querySelector(".OmiljeniProizvodiKontejner").querySelector(".DugmiciStranica");
    let KontejnerProizvoda = document.querySelector(".OmiljeniProizvodiKontejner").querySelector(".OmiljeniProizvodi");
    
    //Izbiri postojece dugmice
    while(KontejnerDugmicaStranica.firstChild)
        KontejnerDugmicaStranica.removeChild(KontejnerDugmicaStranica.firstChild);
    
    let brojStranica = await VratiBrojOmiljenihProizvoda();
    if (brojStranica % 6 == 0) {
        brojStranica--;
    }
    
    brojStranica = brojStranica / 6;
    brojStranica = Math.floor(brojStranica);
    
    for(let i=0; i <= brojStranica; i++){
        let dugmeStranice = document.createElement("button");
        dugmeStranice.innerHTML = i+1;
        dugmeStranice.classList.add("btn","DugmeStranice");
        dugmeStranice.classList.add("btn-outline-secondary");
        dugmeStranice.onclick = (ev) => {
            document.querySelectorAll(".DugmeStranice").forEach(d => {
                d.classList.remove("bg-dark","text-light");
            })
    
            dugmeStranice.classList.add("bg-dark","text-light");
    
            // Brise postojeci sadrzaj na stanici
            while(KontejnerProizvoda.firstChild)
            KontejnerProizvoda.removeChild(KontejnerProizvoda.firstChild);
    
            Vrati6OmiljenihProizvoda((i)*6);
        }
        KontejnerDugmicaStranica.appendChild(dugmeStranice);
        if(i === 0)
        dugmeStranice.click();
    }
}

async function VratiBrojOmiljenihProizvoda(){
    return await FetchVratiBrojOmiljenihProizvoda();
}

async function FetchVratiBrojOmiljenihProizvoda(){

    let odgovor = await fetch(webapi + "VojniProizvodi/VratiBrojOmiljenihProizvoda", {
        method:"POST",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        },
        body: JSON.stringify({
            korisnickoIme : localStorage.getItem("username"),
            nazivProizvoda : null
        })
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    return odgovor;  
}

async function Vrati6OmiljenihProizvoda(polozaj) {

    let listaProizvoda = await FetchVrati6OmiljenihProizvoda(polozaj);
    if (listaProizvoda == null || listaProizvoda == false) {
        return false;
    }

    let kontejnerProizvoda = document.querySelector(".OmiljeniProizvodi");
    kontejnerProizvoda.innerHTML = "";

    listaProizvoda.forEach(proizvod => {
        proizvod.PrikazProizvoda(kontejnerProizvoda);
    })

}

async function FetchVrati6OmiljenihProizvoda(polozaj){


    let odgovor = await fetch(webapi + "VojniProizvodi/Vrati6OmiljenihProizvoda/" + polozaj, {
        method:"POST",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        },
        body: JSON.stringify({
            korisnickoIme : localStorage.getItem("username"),
            nazivProizvoda : null
        })
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    let listaProizvoda  = [];

    odgovor.forEach(proizvod => {
        listaProizvoda.push(new Proizvod(proizvod.id, proizvod.sifraProizvoda, proizvod.naziv, proizvod.kategorija,
             proizvod.kolicina, proizvod.cene, proizvod.slika, proizvod.velicina, proizvod.opis,
            proizvod.velicineKolicine, proizvod.modeli));
    })

    return listaProizvoda;

}

async function VratiBrojProizvoda() {

    return await FetchVratiBrojProizvoda();

}

async function FetchVratiBrojProizvoda() {

    let tekstPretrage = InputPretrazi.value.trim();
    let fleg = "true";
    if (tekstPretrage == "") {
        tekstPretrage = "bla";
        fleg = "false";
    }

    let kategorije = [];

    let checkboxes = document.querySelector(".Filteri").querySelectorAll("input");
    checkboxes.forEach(cbx => {
        if (cbx.checked == true) {
            kategorije.push(cbx.value);
        }
    })
    if (kategorije.length == 0) {
        checkboxes.forEach(cbx => {
            kategorije.push(cbx.value);
        })
    }

    let odgovor = await fetch(webapi + "VojniProizvodi/VratiBrojPretrazeneProizvode/" + tekstPretrage + "/" + fleg, {
        method: 'POST',
        headers: {
            "Content-Type" : "application/json"
        },
        body: JSON.stringify(kategorije)
    }).catch(reason =>  { 
        return false; 
    });

    if (odgovor == false) {

        return false;

    }

    odgovor = await odgovor.json().catch(reason => {

        return false;

    });

    if (odgovor == false) {

        return false;

    }

    return odgovor;
}

var formaZaPrijavljivanje = document.querySelector(".FormaZaPrijavljivanje");
var formaZaRegistraciju = document.querySelector(".FormaRegistracija");
var formaZaPromenuSifre = document.querySelector(".FormaPromeniSifru");
var kontejnerOmiljenih = document.querySelector(".OmiljeniProizvodiKontejner");

/*function LosToken() {

    localStorage.removeItem("token");
    localStorage.removeItem("username");
    window.location.reload();

}*/

DugmeOmiljeno.onclick = () => {

    formaZaPrijavljivanje.setAttribute('style','display: none !important');
    formaZaRegistraciju.setAttribute('style','display: none !important');
    formaZaPromenuSifre.setAttribute('style','display: none !important');
    kontejnerOmiljenih.style.display = "flex";

    GenerisiStraniceOmiljenih();

}

document.querySelector(".PrijaviSe").onclick = (ev) => {
    formaZaPrijavljivanje.style.display = "flex";
    formaZaRegistraciju.setAttribute('style','display: none !important');
    formaZaPromenuSifre.setAttribute('style','display: none !important');
    kontejnerOmiljenih.setAttribute('style','display: none !important');
}

document.querySelector(".PromeniSifru").onclick = (ev) => {
    formaZaPrijavljivanje.setAttribute('style','display: none !important');
    formaZaRegistraciju.setAttribute('style','display: none !important');
    formaZaPromenuSifre.style.display = "flex";
    kontejnerOmiljenih.setAttribute('style','display: none !important');
}

document.querySelector(".RegistrujSe").onclick = (ev) => {
    formaZaPrijavljivanje.setAttribute('style','display: none !important');
    formaZaRegistraciju.style.display = "flex";
    formaZaPromenuSifre.setAttribute('style','display: none !important');
    kontejnerOmiljenih.setAttribute('style','display: none !important');
}


document.querySelector(".DugmePrijaviSe").onclick = (ev) => {
    PrijaviSe();
}

async function PrijaviSe() {

    let fp = document.querySelector(".FormaZaPrijavljivanje");
        fp.querySelectorAll(".invalid-feedback").forEach(p => {
            p.style.display = "none";
        })

    let korisnickoIme = fp.querySelector("input[name=KorisnickoIme]").value;
    let sifra = fp.querySelector("input[name=Sifra]").value;

    if(korisnickoIme == "" || sifra == ""){

        document.querySelector(".PrijavljivanjePopuniteSvaPolja").style.display = "block";
        return;

    }


    PrijavaFetch(korisnickoIme, sifra, fp);    

}

async function PrijavaFetch(korisnickoIme, sifra, fp) {

    let odgovor = await fetch(webapi + "VojniProizvodi/Prijava", {
        method: "POST",
        headers:{
            "Content-Type" : "application/json"
        },
        body: JSON.stringify({
            korisnickoIme : korisnickoIme,
            sifra : sifra
        })
    }).catch(reason => {

        
        return false;

    })

    if (odgovor == false){
        fp.querySelector(".PrijavljivanjeDosloJeDoGreske").style.display = "block";
        return false;
    }
        
    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        fp.querySelector(".PrijavljivanjeDosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Korisnik sa ovim korisnickim imenom ne postoji") {
        document.querySelector(".PrijavljivanjeKorisnickoImeFeedback").style.display = "block";
    }
    else {
        if (odgovor == "Netacna sifra") {
            document.querySelector(".PrijavljivanjeSifraFeedback").style.display = "block";
        }
        else {

            localStorage.setItem("token",odgovor);
            localStorage.setItem("username",korisnickoIme);
            if (korisnickoIme == "admin") {
                window.open("administrator.html", '_blank','noopener noreferrer');
            }
            document.querySelector(".PrijaviSe").style.display = "none";
            formaZaPrijavljivanje.setAttribute('style','display: none !important');
            document.querySelector(".PrijavljeniKorisnik").innerHTML = korisnickoIme;
            document.querySelector(".OdjaviSe").style.display = "block";
            document.querySelector(".PromeniSifru").style.display = "block";
            document.querySelector(".Omiljeno").style.display = "block";
            document.querySelector(".RegistrujSe").style.display = "none";
            document.querySelector(".PrikazOdabranog").childNodes.forEach(c => {
                if (c.nodeType == 1)
                    c.setAttribute('style','display: none !important');
            })
        }
    }

}

document.querySelector(".OdjaviSe").onclick = (ev) => {
    OdjaviSe();
}

function OdjaviSe() {
    localStorage.removeItem("token");
    localStorage.removeItem("username");
    document.querySelector(".PrijaviSe").style.display = "block";
    document.querySelector(".PromeniSifru").style.display = "none";
    document.querySelector(".Omiljeno").style.display = "none";
    document.querySelector(".PrijavljeniKorisnik").innerHTML = "";
    document.querySelector(".OdjaviSe").style.display = "none";
    document.querySelector(".RegistrujSe").style.display = "block";

    document.querySelector(".PrikazOdabranog").childNodes.forEach(c => {
        if (c.nodeType == 1)
            c.setAttribute('style','display: none !important');
    })
}



let dugmePromeniSifru = document.querySelector(".DugmePromeniSifru");
dugmePromeniSifru.onclick = (ev) => {
    PromeniSifru();
}

async function PromeniSifru() {

    let forma = document.querySelector(".FormaPromeniSifru");

    let sifra = forma.querySelector("input[name=Sifra]").value;
    let novaSifra = forma.querySelector("input[name=NovaSifra]").value;
    let potvrdjenaNovaSifra = forma.querySelector("input[name=PotvrdjenaSifra]").value;

    forma.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    forma.querySelector(".Uspesno").style.display = "none";

    if (sifra == "" || novaSifra == "" || potvrdjenaNovaSifra == "") {
        forma.querySelector(".PrijavljivanjePopuniteSvaPolja").style.display = "block";
        return;
    }

    await FetchPromeniSifru(sifra, novaSifra, potvrdjenaNovaSifra);

}

async function FetchPromeniSifru(sifra, novaSifra, potvrdjenaNovaSifra) {

    let forma = document.querySelector(".FormaPromeniSifru");

    let odgovor = await fetch(webapi+"VojniProizvodi/PromeniSifru",{
        method:"POST",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": localStorage.getItem("token")
        },
        body: JSON.stringify({
            korisnickoIme : localStorage.getItem("username"),
            sifra : sifra,
            novaSifra : novaSifra,
            potvrdjenaSifra : potvrdjenaNovaSifra
        })
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        forma.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        forma.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Los token") {
        LosToken();
        return;
    }

    if(odgovor == "Sifra mora imati bar 8 karaktera"){
        forma.querySelector(".PrijavljivanjeNovaSifraFeedback").style.display = "block";
        return;
    }

    if (odgovor == "Sifra i potvrdjena sifra se ne poklapaju") {
        forma.querySelector(".PrijavljivanjePotvrdjenaSifraFeedback").style.display = "block";
        return;
    }

    if (odgovor == "Netacna sifra") {
        forma.querySelector(".PrijavljivanjeSifraFeedback").style.display = "block";
        return;
    }

    localStorage.setItem("token", odgovor);
    forma.querySelector(".Uspesno").style.display = "block";

}

document.querySelector(".DugmeRegistrujSe").onclick = (ev) => {
    RegistrujSe();
}

async function RegistrujSe() {

    let forma = document.querySelector(".FormaRegistracija");

    let korisnickoIme = forma.querySelector("input[name=KorisnickoIme]").value;
    let novaSifra = forma.querySelector("input[name=NovaSifra]").value;
    let potvrdjenaNovaSifra = forma.querySelector("input[name=PotvrdjenaSifra]").value;

    forma.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    forma.querySelector(".Uspesno").style.display = "none";

    if (korisnickoIme == "" || novaSifra == "" || potvrdjenaNovaSifra == "") {
        forma.querySelector(".PrijavljivanjePopuniteSvaPolja").style.display = "block";
        return;
    }

    await FetchRegistrujSe(korisnickoIme, novaSifra, potvrdjenaNovaSifra);

}

async function FetchRegistrujSe(korisnickoIme, novaSifra, potvrdjenaNovaSifra) {

    let forma = document.querySelector(".FormaRegistracija");

    let odgovor = await fetch(webapi+"VojniProizvodi/Registracija",{
        method:"POST",
        headers:{
            "Content-Type" : "application/json"
        },
        body: JSON.stringify({
            korisnickoIme: korisnickoIme,
            sifra : novaSifra,
            potvrdjenaSifra : potvrdjenaNovaSifra
        })
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        forma.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }

    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        forma.querySelector(".DosloJeDoGreske").style.display = "block";
        return false;
    }


    if (odgovor == "Korisnicko ime mora imati bar 8 karaktera ili sadrzi razmake") {
        forma.querySelector(".PrijavljivanjeKorisnickoImeFeedback").style.display = "block";
        return;
    }
    
    if (odgovor == "Username zauzet") {
        forma.querySelector(".PrijavljivanjeKorisnickoIme2Feedback").style.display = "block";
        return;
    }

    if(odgovor == "Sifra mora imati bar 8 karaktera"){
        forma.querySelector(".PrijavljivanjeNovaSifraFeedback").style.display = "block";
        return;
    }

    if (odgovor == "Sifra i potvrdjena sifra se ne poklapaju") {
        forma.querySelector(".PrijavljivanjePotvrdjenaSifraFeedback").style.display = "block";
        return;
    }

    if (odgovor == "Uspesna registracija") {
        forma.querySelector(".Uspesno").style.display = "block";
    }

}

